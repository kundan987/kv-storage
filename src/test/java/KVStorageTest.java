import service.impl.KVStorageServiceImpl;

public class KVStorageTest {

    public static void main(String[] args) throws Exception{
        KVStorageServiceImpl kvStorageService;
        StartUpService startUpService = new StartUpService();
        kvStorageService = startUpService.init();


       kvStorageService.insert("1", "kundan");
        kvStorageService.insert("2", "Ram");
        kvStorageService.insert("3", "Shaym");
        kvStorageService.insert("4", "Hello");
        kvStorageService.delete("2");
        kvStorageService.update("3", "good morning");
        kvStorageService.insert("5", "{\"id\":1,\"content\":\"Hello, World!\"}");
        System.out.println(kvStorageService.getMap());
    }
}
