package service;

public interface KVStorageService {

    void insert(String key, String value);
    String get(String key);
    void update(String key, String value) throws Exception;
    void delete(String key) throws Exception;
}
