package service.impl;

import enums.Type;
import service.KVStorageService;
import storage.StorageService;

import java.util.Map;

public class KVStorageServiceImpl implements KVStorageService {

    Map<String, String> map;

    StorageService storageService;

    public Map<String, String> getMap() {
        return map;
    }

    public KVStorageServiceImpl(Map<String, String> map, StorageService storageService) {
        this.map = map;
        this.storageService = storageService;
    }

    public void insert(String key, String value) {

        map.putIfAbsent(key, value);
        storageService.writeToFile(key, value, Type.WRITE);
    }

    public String get(String key) {
        return map.get(key);
    }

    public void update(String key, String value) throws Exception{
        if (map.containsKey(key)){
            map.put(key, value);
            storageService.writeToFile(key, value, Type.UPDATE);
        } else
            throw new Exception("Key not found!");

    }

    public void delete(String key) throws Exception{
        if (map.containsKey(key)){
            map.remove(key);
            storageService.writeToFile(key, null, Type.DELETE);
        } else
            throw new Exception("Key not found!");
    }
}
