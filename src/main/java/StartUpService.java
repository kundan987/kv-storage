import enums.Type;
import service.impl.KVStorageServiceImpl;
import storage.StorageService;

import java.io.*;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

public class StartUpService {

    public KVStorageServiceImpl init(){
        String pathname = "data.txt";
        File file = new File(pathname);
        KVStorageServiceImpl kvStorageService = null;
        try {
            Map<String, String> map = new ConcurrentHashMap<>();
            try {
                FileReader fileReader = new FileReader(file);
                backup(map, fileReader);
            } catch (FileNotFoundException e){
                System.out.println("No data");
            }

            FileWriter fileWriter = new FileWriter(file, true);
            StorageService storageService = new StorageService(fileWriter);
            kvStorageService = new KVStorageServiceImpl(map, storageService);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return kvStorageService;
    }

    private void backup(Map<String, String> map, FileReader fileReader){

        try (Scanner scanner = new Scanner(fileReader)) {
            while (scanner.hasNextLine()){
                String readLine = scanner.nextLine();
                String[] data = readLine.split(":::");
                if (String.valueOf(Type.DELETE).equalsIgnoreCase(data[0])){
                    map.remove(data[1]);
                }else
                    map.put(data[1], data[2]);
            }
        }
    }

}
