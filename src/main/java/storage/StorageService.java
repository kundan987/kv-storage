package storage;

import enums.Type;

import java.io.FileWriter;
import java.io.IOException;

public class StorageService {
    FileWriter fileWriter;

    public StorageService(FileWriter fileWriter) {
        this.fileWriter = fileWriter;
    }

    public void writeToFile(String key, String value, Type type) {


        StringBuilder sb = new StringBuilder().append(type).append(":::").append(key).append(":::").append(value).append("\n");
        try{
            fileWriter.write(new String(sb));
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
